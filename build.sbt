name := "SyncNotes"

version := "1.0"

lazy val `syncnotes` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
    jdbc
    , anorm
    , cache
    , ws
    , "org.webjars" %% "webjars-play" % "2.3.0-2"
    , "org.xerial" % "sqlite-jdbc" % "3.7.2"
    , "ws.securesocial" %% "securesocial" % "master-SNAPSHOT"
    )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

