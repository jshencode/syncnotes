package controllers

import models.{NoteData, Note}
import play.api.data._
import play.api.libs.json.Json
import play.api.mvc._
import play.api.data.Forms._

import scala.concurrent.Future

/**
 * Created by jians on 2/11/15.
 */
object Notes extends Controller {

    def get(id: Int) = Action.async {
        Note.findById(id) map {
            case Some(note : Note) => Ok(Json.toJson(note))
            case _  => NoContent
        }
    }

    def getAll = Action.async {
        Note.findAll map {
            case list: List[Note] => Ok(Json.toJson(list))
            case _ => NoContent
        }
    }

    def post = Action.async {
        implicit request =>

        Note.noteForm.bindFromRequest match {
            case form : Form[NoteData] if !form.hasErrors => {
                val noteData = form.get
                Note.create(noteData.title, noteData.content) map {
                    case Some(id: Long) => Created(Json.obj("created" -> id))
                    case _ => InternalServerError(Json.obj("created" -> false))
                }
            }
            case _ => Future { BadRequest(Json.obj("reason" -> "Could not bind post data to form"))}
        }

    }


}
