package models

import anorm._
import anorm.SqlParser._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json.Json

import scala.concurrent.Future

/**
 * Created by jians on 2/11/15.
 */

case class NoteData(title:String, content:String)
case class Note(title:String, content:String, created:DateTime)

object Note {

    implicit val jsonFormat = Json.format[Note]

    val noteForm = Form(
        mapping(
            "title" -> text,
            "content" -> text
        )(NoteData.apply)(NoteData.unapply)
    )

    val notes =
        str("title")~
        str("content")~
        long("created") map {
            case title~content~created => Note(title, content, new DateTime(created))
        }

    def findById(id: Int) = Future {
        DB.withConnection {
            implicit connection =>
            SQL(
                """
                   SELECT title, content, created
                   FROM note
                   WHERE id = {id};
                """
            )
            .on(
                'id -> id
            ).as(notes.singleOpt)
        }
    }

    def findAll = Future {
        DB.withConnection {
            implicit connection =>
            SQL(
                """
                    SELECT title, content, created
                    FROM note
                """
            ).as(
                notes *
            )
        }
    }


    def create(title:String, content: String) = Future {
        val created = now
        DB.withConnection {
            implicit connection =>
            SQL(
                """
                    INSERT INTO note (
                        title,
                        content,
                        created
                    ) VALUES (
                        {title},
                        {content},
                        {created}
                    );
                """
            ).on(
                'title -> title,
                'content -> content,
                'created -> created
            ).executeInsert()
        }

    }

}
