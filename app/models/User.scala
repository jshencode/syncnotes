package models

import anorm._
import anorm.SqlParser._
import play.api.data._
import play.api.data.Forms._

import scala.concurrent.Future

/**
 * Created by jians on 2/13/15.
 */
case class UserData(username: String, email: String, password: String)
case class User(id: Int, username: String, email: String, password: String)

object User {

    val userForm = Form (
        mapping(
            "username" -> text,
            "email" -> email,
            "password" -> text
        )(UserData.apply)(UserData.unapply)
    )

    val userParser = {
        int("id")~
        str("username")~
        str("email")~
        str("password") map {
            case id~username~email~password => User(id, username, email, password)
        }
    }

    def findByName(userName:String) = Future {
        DB.withConnection {
            implicit connection =>
            SQL(
                """
                    SELECT *
                    FROM users
                    WHERE username = {username}
                """
            ).on(
                'username -> userName
            ).as(
                userParser.singleOpt
            )
        }
    }

}

