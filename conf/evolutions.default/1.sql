# Notes schema

# --- !Ups
CREATE TABLE note (
  id        INTEGER       NOT NULL PRIMARY KEY AUTOINCREMENT,
  title     VARCHAR(255)  NOT NULL,
  content   TEXT          NOT NULL,
  created   DATETIME      NOT NULL
);

# --- !Downs
DROP TABLE note;